package Operatii;

public class Inmultire{
	public int[] arr1 = new int[50];
	public int[] arr2 = new int[50];
	private int ok = 1;
	public String result = " ";
	private int size1 = 0, size2 = 0;

	public Inmultire(String polinom1, String polinom2) {
		Verifica(polinom1, polinom2);
		if (ok == 1) {
			ImparteInNr(polinom1, polinom2);
			Inmulteste(arr1, arr2);
		} else
			result = "Format invalid";
	}

	public void Verifica(String polinom1, String polinom2) {
		for (int i = 0; i < polinom1.length(); i++) {
			if (!((polinom1.charAt(i) >= '0' && polinom1.charAt(i) <= '9') || polinom1.charAt(i) == ' '
					|| polinom1.charAt(i) == ',' || polinom1.charAt(i) == '-'))
				ok = 0;
		}
		for (int i = 0; i < polinom2.length(); i++) {
			if (!((polinom2.charAt(i) >= '0' && polinom2.charAt(i) <= '9') || polinom2.charAt(i) == ' '
					|| polinom2.charAt(i) == ',' || polinom2.charAt(i) == '-'))
				ok = 0;
		}
		if ((polinom1.charAt(0) < '0' || polinom1.charAt(0) > '9') && polinom1.charAt(0) != '-')
			ok = 0;
		if ((polinom2.charAt(0) < '0' || polinom2.charAt(0) > '9') && polinom2.charAt(0) != '-')
			ok = 0;
	}

	public void ImparteInNr(String polinom1, String polinom2) {
		String[] monom1 = polinom1.split("[ ,]+");
		String[] monom2 = polinom2.split("[ ,]+");
		for (String monom : monom1) {
			arr1[size1] = Integer.parseInt(monom);
			size1++;
		}

		for (String monom : monom2) {
			arr2[size2] = Integer.parseInt(monom);
			size2++;
		}
	}

	public void Inmulteste(int[] s1, int[] s2) {
		int grad = size1 + size2 - 1;
		int putere = grad - 1;
		int[] coef = new int[grad];
		for (int i = 0; i < grad; i++) {
			for (int j = 0; j < size1; j++) {
				for (int k = 0; k < size2; k++)
					if (j + k == i)
						coef[i] += s1[j] * s2[k];
			}
		}
		for (int i = 0; i < grad - 1; i++) {
			if (coef[i] != 0)
				result = result + "" + coef[i] + "x^" + putere + "+";
			putere--;
		}
		if (coef[grad - 1] != 0)
			result = result + "" + coef[grad - 1];
		result = result.replaceAll("\\+-", "-");
		result = result.replaceAll("\\^1\\+", "+");
		result = result.replaceAll("\\^1\\-", "-");
		result = result.replaceAll("\\+1x", "+x");
		result = result.replaceAll("\\-1x", "-x");
		result = result.replaceAll("\\ 1x", " x");
		if (result.charAt(result.length() - 1) == '+')
			result = result.substring(0, result.length() - 1);
		if (result.equals(" "))
			result = "0";
	}
}
