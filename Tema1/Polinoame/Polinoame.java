package Polinoame;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.*;
import javax.swing.*;

import Operatii.*;

public class Polinoame implements ActionListener{

	public JFrame calculatorPolin;
	public JLabel PrimulPolinom, AlDoileaPolinom, Rezultat;
	public JTextField tfPrimul,tfDoilea;
	public JTextField tfRezultat;
	JButton jbnAdd, jbnSubtract, jbnClear, jbnMultiply, jbnDivide, jbnDerivate, jbnIntegrate;
	Container cPane;

	public Polinoame() {
		createGUI();
	}

	public void createGUI() {
		calculatorPolin = new JFrame("Calculator Polinoame");
		cPane = calculatorPolin.getContentPane();
		cPane.setLayout(new GridBagLayout());
		arrangeComponents();
		calculatorPolin.pack();
		calculatorPolin.setVisible(true);
	}

	public void arrangeComponents() {

		PrimulPolinom = new JLabel("Primul polinom");
		AlDoileaPolinom = new JLabel("Al doilea polinom");
		Rezultat = new JLabel("Rezultat");

		tfPrimul = new JTextField(20);
		tfDoilea = new JTextField(20);
		tfRezultat = new JTextField(20);

		jbnAdd = new JButton("+");
		jbnSubtract = new JButton("-");
		jbnMultiply = new JButton("*");
		jbnDivide = new JButton("/");
		jbnDerivate = new JButton("Derivata");
		jbnIntegrate = new JButton("Integrala");
		jbnClear = new JButton("Clear");

		GridBagConstraints gridBagConstraintsx01 = new GridBagConstraints();
		gridBagConstraintsx01.gridx = 0;
		gridBagConstraintsx01.gridy = 0;
		cPane.add(PrimulPolinom, gridBagConstraintsx01);

		GridBagConstraints gridBagConstraintsx02 = new GridBagConstraints();
		gridBagConstraintsx02.gridx = 1;
		gridBagConstraintsx02.gridy = 0;
		gridBagConstraintsx02.gridwidth = 2;
		cPane.add(tfPrimul, gridBagConstraintsx02);

		GridBagConstraints gridBagConstraintsx03 = new GridBagConstraints();
		gridBagConstraintsx03.gridx = 0;
		gridBagConstraintsx03.gridy = 1;
		cPane.add(AlDoileaPolinom, gridBagConstraintsx03);

		GridBagConstraints gridBagConstraintsx04 = new GridBagConstraints();
		gridBagConstraintsx04.gridx = 1;
		gridBagConstraintsx04.gridy = 1;
		gridBagConstraintsx04.gridwidth = 2;
		gridBagConstraintsx04.fill = GridBagConstraints.BOTH;
		cPane.add(tfDoilea, gridBagConstraintsx04);

		GridBagConstraints gridBagConstraintsx05 = new GridBagConstraints();
		gridBagConstraintsx05.gridx = 0;
		gridBagConstraintsx05.insets = new Insets(5, 5, 5, 5);
		gridBagConstraintsx05.gridy = 2;
		cPane.add(Rezultat, gridBagConstraintsx05);

		GridBagConstraints gridBagConstraintsx06 = new GridBagConstraints();
		gridBagConstraintsx06.gridx = 1;
		gridBagConstraintsx06.gridy = 2;
		gridBagConstraintsx06.insets = new Insets(5, 5, 5, 5);
		gridBagConstraintsx06.gridwidth = 2;
		gridBagConstraintsx06.fill = GridBagConstraints.BOTH;
		cPane.add(tfRezultat, gridBagConstraintsx06);

		GridBagConstraints gridBagConstraintsx09 = new GridBagConstraints();
		gridBagConstraintsx09.gridx = 0;
		gridBagConstraintsx09.gridy = 4;
		cPane.add(jbnAdd, gridBagConstraintsx09);

		GridBagConstraints gridBagConstraintsx10 = new GridBagConstraints();
		gridBagConstraintsx10.gridx = 1;
		gridBagConstraintsx10.gridy = 4;
		cPane.add(jbnSubtract, gridBagConstraintsx10);

		GridBagConstraints gridBagConstraintsx11 = new GridBagConstraints();
		gridBagConstraintsx11.gridx = 2;
		gridBagConstraintsx11.gridy = 4;
		cPane.add(jbnMultiply, gridBagConstraintsx11);

		GridBagConstraints gridBagConstraintsx12 = new GridBagConstraints();
		gridBagConstraintsx12.gridx = 3;
		gridBagConstraintsx12.gridy = 4;
		cPane.add(jbnDivide, gridBagConstraintsx12);

		GridBagConstraints gridBagConstraintsx13 = new GridBagConstraints();
		gridBagConstraintsx13.gridx = 1;
		gridBagConstraintsx13.gridy = 5;
		cPane.add(jbnDerivate, gridBagConstraintsx13);

		GridBagConstraints gridBagConstraintsx14 = new GridBagConstraints();
		gridBagConstraintsx14.gridx = 2;
		gridBagConstraintsx14.gridy = 5;
		cPane.add(jbnIntegrate, gridBagConstraintsx14);

		GridBagConstraints gridBagConstraintsx15 = new GridBagConstraints();
		gridBagConstraintsx15.gridy = 6;
		cPane.add(jbnClear, gridBagConstraintsx15);

		jbnAdd.addActionListener(this);
		jbnSubtract.addActionListener(this);
		jbnClear.addActionListener(this);
		jbnMultiply.addActionListener(this);
		jbnDivide.addActionListener(this);
		jbnDerivate.addActionListener(this);
		jbnIntegrate.addActionListener(this);

	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == jbnAdd) {
			String first = tfPrimul.getText();
			String second = tfDoilea.getText();
			if (first.length() == 0 || second.length() == 0)
				tfRezultat.setText("Introduceti doua polinoame");
			else {
				Adunare rezultat = new Adunare(first, second);
				tfRezultat.setText(rezultat.result);
			}

		} else if (e.getSource() == jbnSubtract) {
			String first = tfPrimul.getText();
			String second = tfDoilea.getText();
			if (first.length() == 0 || second.length() == 0)
				tfRezultat.setText("Introduceti doua polinoame");
			else {
				Scadere rezultat = new Scadere(first, second);
				tfRezultat.setText(rezultat.result);
			}
			
		} else if (e.getSource() == jbnClear) {
			clear();
			
		} else if (e.getSource() == jbnMultiply) {
			String first = tfPrimul.getText();
			String second = tfDoilea.getText();
			if (first.length() == 0 || second.length() == 0)
				tfRezultat.setText("Introduceti doua polinoame");
			else {
				Inmultire rezultat = new Inmultire(first, second);
				tfRezultat.setText(rezultat.result);
			}
			
		} else if (e.getSource() == jbnDivide) {
			String first = tfPrimul.getText();
			String second = tfDoilea.getText();
			if (first.length() == 0 || second.length() == 0)
				tfRezultat.setText("Introduceti doua polinoame");
			else {
				Impartire rezultat = new Impartire(first, second);
				tfRezultat.setText(rezultat.result);
			}
			
		} else if (e.getSource() == jbnIntegrate) {
			String first = tfPrimul.getText();
			if (first.length() == 0)
				tfRezultat.setText("Introduceti primul polinom");
			else {
				Integrare rezultat = new Integrare(first);
				tfRezultat.setText(rezultat.result);
			}
			
		} else if (e.getSource() == jbnDerivate) {
			String first = tfPrimul.getText();
			if (first.length() == 0)
				tfRezultat.setText("Introduceti primul polinom");
			else {
				Derivare rezultat = new Derivare(first);
				tfRezultat.setText(rezultat.result);
			}
		}
		
	}

	public void clear() {
		tfPrimul.setText("");
		tfDoilea.setText("");
		tfRezultat.setText("");
	}
}