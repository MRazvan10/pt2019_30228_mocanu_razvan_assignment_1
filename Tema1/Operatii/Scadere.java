package Operatii;

public class Scadere{
	public int[] arr1 = new int[50];
	public int[] arr2 = new int[50];
	private int ok = 1;
	public String result = " ";
	private int size1 = 0, size2 = 0;

	public Scadere(String polinom1, String polinom2) {
		Verifica(polinom1, polinom2);
		if (ok == 1) {
			ImparteInNr(polinom1, polinom2);
			Scade(arr1, arr2);
		} else
			result = "Format invalid";
	}

	public void Verifica(String polinom1, String polinom2) {
		for (int i = 0; i < polinom1.length(); i++) {
			if (!((polinom1.charAt(i) >= '0' && polinom1.charAt(i) <= '9') || polinom1.charAt(i) == ' '
					|| polinom1.charAt(i) == ',' || polinom1.charAt(i) == '-'))
				ok = 0;
		}
		for (int i = 0; i < polinom2.length(); i++) {
			if (!((polinom2.charAt(i) >= '0' && polinom2.charAt(i) <= '9') || polinom2.charAt(i) == ' '
					|| polinom2.charAt(i) == ',' || polinom2.charAt(i) == '-'))
				ok = 0;
		}
		if ((polinom1.charAt(0) < '0' || polinom1.charAt(0) > '9') && polinom1.charAt(0) != '-')
			ok = 0;
		if ((polinom2.charAt(0) < '0' || polinom2.charAt(0) > '9') && polinom2.charAt(0) != '-')
			ok = 0;
	}

	public void ImparteInNr(String polinom1, String polinom2) {
		String[] monom1 = polinom1.split("[ ,]+");
		String[] monom2 = polinom2.split("[ ,]+");
		for (String monom : monom1) {
			arr1[size1] = Integer.parseInt(monom);
			size1++;
		}

		for (String monom : monom2) {
			arr2[size2] = Integer.parseInt(monom);
			size2++;
		}
	}

	public void Scade(int[] s1, int[] s2) {
		int grad;
		if (size1 > size2)
			grad = size1;
		else
			grad = size2;
		int putere = grad - 1;
		int graddif;
		int j = 0;
		if (size1 > size2) {
			graddif = size1 - size2;
			for (int i = 0; i < graddif; i++) {
				if (s1[i] != 1 && s1[i] != 0 && i != (grad - 1))
					result = result + "" + s1[i] + "x^" + putere + "+";
				if (s1[i] == 1 && i != (grad - 1))
					result = result + "" + "x^" + putere + "+";
				if (s1[i] != 0 && i == (grad - 1))
					result = result + "" + s1[i];
				putere--;
			}
			for (int i = graddif; i < grad; i++) {
				int dif = s1[i] - s2[j];
				if (dif != 1 && dif != 0 && i != (grad - 1))
					result = result + "" + dif + "x^" + putere + "+";
				if (dif == 1 && i != (grad - 1))
					result = result + "" + "x^" + putere + "+";
				if (dif != 0 && i == (grad - 1))
					result = result + "" + dif;
				putere--;
				j++;
			}
		} else if (size2 > size1) {
			graddif = size2 - size1;
			for (int i = 0; i < graddif; i++) {
				if (s2[i] != 1 && s1[i] != 0 && i != (grad - 1))
					result = result + "-" + s2[i] + "x^" + putere + "+";
				if (s2[i] == 1 && i != (grad - 1))
					result = result + "-" + "x^" + putere + "+";
				if (s2[i] != 0 && i == (grad - 1))
					result = result + "-" + s2[i];
				putere--;
			}
			for (int i = graddif; i < grad; i++) {
				int dif = s1[j] - s2[i];
				if (dif != 1 && dif != 0 && i != (grad - 1))
					result = result + "" + dif + "x^" + putere + "+";
				if (dif == 1 && i != (grad - 1))
					result = result + "" + "x^" + putere + "+";
				if (dif != 0 && i == (grad - 1))
					result = result + "" + dif;
				putere--;
				j++;
			}
		} else
			for (int i = 0; i < grad; i++) {
				int dif = s1[i] - s2[i];
				if (dif != 1 && dif != 0 && i != (grad - 1))
					result = result + "" + dif + "x^" + putere + "+";
				if (dif == 1 && i != (grad - 1))
					result = result + "" + "x^" + putere + "+";
				if (dif != 0 && i == (grad - 1))
					result = result + "" + dif;
				putere--;
			}
		result = result.replaceAll("\\+-", "-");
		result = result.replaceAll("\\^1\\+", "+");
		result = result.replaceAll("\\^1\\-", "-");
		result = result.replaceAll("\\+1x", "+x");
		result = result.replaceAll("\\-1x", "-x");
		result = result.replaceAll("\\ 1x", " x");
		if (result.charAt(result.length() - 1) == '+')
			result = result.substring(0, result.length() - 1);
		if (result.equals(" "))
			result = "0";
	}
}