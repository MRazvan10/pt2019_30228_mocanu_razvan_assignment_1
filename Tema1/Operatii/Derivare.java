package Operatii;

public class Derivare {
	public int[] arr1 = new int[50];
	private int ok = 1;
	public String result = " ";
	private int size1 = 0;

	public Derivare(String polinom1) {
		for (int i = 0; i < polinom1.length(); i++) {
			if (!((polinom1.charAt(i) >= '0' && polinom1.charAt(i) <= '9') || polinom1.charAt(i) == ' '
					|| polinom1.charAt(i) == ',' || polinom1.charAt(i) == '-'))
				ok = 0;
		}
		if ((polinom1.charAt(0) < '0' || polinom1.charAt(0) > '9') && polinom1.charAt(0) != '-')
			ok = 0;
		if (ok == 1) {
			String[] monom1 = polinom1.split("[ ,]+");
			for (String monom : monom1) {
				arr1[size1] = Integer.parseInt(monom);
				size1++;
			}
			Deriveaza(arr1);
		} else
			result = "Format invalid";
	}

	public void Deriveaza(int[] s1) {
		if (size1 != 1) {
			int[] coef = new int[size1];
			int putere = size1 - 2;
			for (int i = 0; i < size1 - 1; i++) {
				coef[i] = s1[i] * (size1 - i - 1);
			}
			for (int i = 0; i < size1 - 2; i++) {
				if (coef[i] != 0)
					result = result + coef[i] + "x^" + putere + "+";
				putere--;
			}
			if (coef[size1 - 2] != 0)
				result = result + coef[size1 - 2];
			if (result.equals(" "))
				result = "0";
			result = result.replaceAll("\\+-", "-");
			result = result.replaceAll("\\^1\\+", "+");
			result = result.replaceAll("\\^1\\-", "-");
			result = result.replaceAll("\\+1x", "+x");
			result = result.replaceAll("\\-1x", "-x");
			result = result.replaceAll("\\ 1x", " x");
			if (result.charAt(result.length() - 1) == '+')
				result = result.substring(0, result.length() - 1);
		} else
			result = "0";
	}
}
