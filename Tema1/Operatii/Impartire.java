package Operatii;

public class Impartire {
	public int[] arr1 = new int[50];
	public int[] arr2 = new int[50];
	private int ok = 1;
	public String result = " ";
	private int size1 = 0, size2 = 0;

	public Impartire(String polinom1, String polinom2) {
		Verifica(polinom1, polinom2);
		if (ok == 1) {
			ImparteInNr(polinom1, polinom2);
			Imparte(arr1, arr2);
		} else
			result = "Format invalid";
	}

	public void Verifica(String polinom1, String polinom2) {
		for (int i = 0; i < polinom1.length(); i++) {
			if (!((polinom1.charAt(i) >= '0' && polinom1.charAt(i) <= '9') || polinom1.charAt(i) == ' '
					|| polinom1.charAt(i) == ',' || polinom1.charAt(i) == '-'))
				ok = 0;
		}
		for (int i = 0; i < polinom2.length(); i++) {
			if (!((polinom2.charAt(i) >= '0' && polinom2.charAt(i) <= '9') || polinom2.charAt(i) == ' '
					|| polinom2.charAt(i) == ',' || polinom2.charAt(i) == '-'))
				ok = 0;
		}
		if ((polinom1.charAt(0) < '0' || polinom1.charAt(0) > '9') && polinom1.charAt(0) != '-')
			ok = 0;
		if ((polinom2.charAt(0) < '0' || polinom2.charAt(0) > '9') && polinom2.charAt(0) != '-')
			ok = 0;
	}

	public void ImparteInNr(String polinom1, String polinom2) {
		String[] monom1 = polinom1.split("[ ,]+");
		String[] monom2 = polinom2.split("[ ,]+");
		for (String monom : monom1) {
			arr1[size1] = Integer.parseInt(monom);
			size1++;
		}

		for (String monom : monom2) {
			arr2[size2] = Integer.parseInt(monom);
			size2++;
		}
	}

	public void Imparte(int[] s1, int[] s2) {
		result = result + "Catul este: ";
		ImparteRec(s1, s2);
		result = result.replaceAll("\\+-", "-");
		result = result.replaceAll("\\^1", "");
		result = result.replaceAll("\\+1.0x", "+x");
		result = result.replaceAll("\\-1.0x", "-x");
		result = result.replaceAll("\\ 1.0x", " x");
		if (result.charAt(result.length() - 1) == '+')
			result = result.substring(0, result.length() - 1);
		if (result.equals(" "))
			result = "0";
	}

	public void ImparteRec(float[] sir1, int[] sir2) {
		if (size2 > size1)
			result = result + "0";
		else {
			while (size1 >= size2) {
				if (sir2[0] == 0) {
					result = "Nu se poate imparti la 0";
					break;
				}
				float imp = (float) sir1[0] / (float) sir2[0];
				int putere = size1 - size2;
				if (imp != 0 && putere != 0)
					result = result + imp + "x^" + putere + "+";
				if (imp != 0 && putere == 0)
					result = result + imp;
				float coef[] = new float[size1];
				int gradRamas = 99;
				for (int i = 0; i < size2; i++) {
					coef[i] = (float) (sir1[i] - sir2[i] * imp);
					if (coef[i] != 0 && i < gradRamas)
						gradRamas = i;
				}
				for (int i = size2; i < size1; i++) {
					coef[i] = (float) (sir1[i]);
					if (coef[i] != 0 && i < gradRamas)
						gradRamas = i;
				}
				if (size1 - gradRamas >= size2) {
					float[] s = new float[size1 - gradRamas];
					int j = 0;
					for (int i = gradRamas; i < size1; i++) {
						s[j] = coef[i];
						j++;
					}
					size1 -= gradRamas;
					ImparteRec(s, sir2);
				} else {
					String rest = " ; Restul este: ";
					if (gradRamas == 99)
						gradRamas = 0;
					for (int i = gradRamas; i < size1; i++) {
						int j = size1 - i - 1;
						if (coef[i] != 0 && j != 0)
							rest = rest + coef[i] + "x^" + j + "+";
						if (coef[i] != 0 && j == 0)
							rest = rest + coef[i];
					}
					if (!(rest.equals(" ; Restul este: ")))
						result = result + rest;
					gradRamas = 999;
					size1 -= gradRamas;
				}
			}
		}
	}

	public void ImparteRec(int[] sir1, int[] sir2) {
		if (size2 > size1)
			result = result + "0";
		else {
			while (size1 >= size2) {
				if (sir2[0] == 0) {
					result = "Nu se poate imparti la 0";
					break;
				}
				float imp = (float) sir1[0] / (float) sir2[0];
				int putere = size1 - size2;
				if (imp != 0 && putere != 0)
					result = result + imp + "x^" + putere + "+";
				if (imp != 0 && putere == 0)
					result = result + imp;
				float coef[] = new float[size1];
				int gradRamas = 99;
				for (int i = 0; i < size2; i++) {
					coef[i] = (float) (sir1[i] - sir2[i] * imp);
					if (coef[i] != 0 && i < gradRamas)
						gradRamas = i;
				}
				for (int i = size2; i < size1; i++) {
					coef[i] = (float) (sir1[i]);
					if (coef[i] != 0 && i < gradRamas)
						gradRamas = i;
				}

				if (size1 - gradRamas >= size2) {
					float[] s = new float[size1 - gradRamas];
					int j = 0;
					for (int i = gradRamas; i < size1; i++) {
						s[j] = coef[i];
						j++;
					}
					size1 -= gradRamas;
					ImparteRec(s, sir2);
				} else {
					String rest = " ; Restul este: ";
					if (gradRamas == 99)
						gradRamas = 0;
					for (int i = gradRamas; i < size1; i++) {
						int j = size1 - i - 1;
						if (coef[i] != 0 && j != 0)
							rest = rest + coef[i] + "x^" + j + "+";
						if (coef[i] != 0 && j == 0)
							rest = rest + coef[i];
					}
					if (!(rest.equals(" ; Restul este: ")))
						result = result + rest;
					gradRamas = 999;
					size1 -= gradRamas;
				}
			}
		}
	}
}
