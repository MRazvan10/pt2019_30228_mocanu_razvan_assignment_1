package Tests;

import static org.junit.Assert.*;
import org.junit.*;
import Operatii.*;
import Polinoame.*;

public class JUnitTest {

	private static Polinoame polin;
	private static int nrTesteExecutate = 0;
	private static int nrTesteCuSucces = 0;

	public JUnitTest() {
	}

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		polin = new Polinoame();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		System.out.println(
				"S-au executat " + nrTesteExecutate + " teste din care " + nrTesteCuSucces + " au avut succes!");
	}

	@Before
	public void setUp() throws Exception {
		nrTesteExecutate++;
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSetValue() {
		polin.clear();
		polin.tfPrimul.setText("1");
		polin.tfDoilea.setText("1");
		String x = polin.tfPrimul.getText();
		String y = polin.tfDoilea.getText();
		assertNotNull(x);
		assertEquals(x, "1");
		assertNotNull(y);
		assertEquals(y, "1");
		nrTesteCuSucces++;
	}

	@Test
	public void testSetValue2() {
		polin.clear();
		polin.tfPrimul.setText("-1");
		polin.tfDoilea.setText("xyz");
		String x = polin.tfPrimul.getText();
		String y = polin.tfDoilea.getText();
		assertNotNull(x);
		assertEquals(x, "-1");
		assertNotNull(y);
		assertEquals(y, "xyz");
		nrTesteCuSucces++;
	}

	@Test
	public void testMultiply() {
		polin.clear();
		polin.tfPrimul.setText("7 0");
		polin.tfDoilea.setText("1 -1");
		String x = polin.tfPrimul.getText();
		String y = polin.tfDoilea.getText();
		Inmultire rezultat = new Inmultire(x, y);
		assertNotNull(rezultat.result);
		assertEquals(rezultat.result, " 7x^2-7x");
		nrTesteCuSucces++;
	}

	@Test
	public void testMultiply2() {
		polin.clear();
		polin.tfPrimul.setText("0 0");
		polin.tfDoilea.setText("1");
		String x = polin.tfPrimul.getText();
		String y = polin.tfDoilea.getText();
		Inmultire rezultat = new Inmultire(x, y);
		assertNotNull(rezultat.result);
		assertEquals(rezultat.result, "0");
		nrTesteCuSucces++;
	}

	@Test
	public void testAdd() {
		polin.clear();
		polin.tfPrimul.setText("7 0");
		polin.tfDoilea.setText("1 -1");
		String x = polin.tfPrimul.getText();
		String y = polin.tfDoilea.getText();
		Adunare rezultat = new Adunare(x, y);
		assertNotNull(rezultat.result);
		assertEquals(rezultat.result, " 8x-1");
		nrTesteCuSucces++;
	}

	@Test
	public void testAdd2() {
		polin.clear();
		polin.tfPrimul.setText("0");
		polin.tfDoilea.setText("1");
		String x = polin.tfPrimul.getText();
		String y = polin.tfDoilea.getText();
		Adunare rezultat = new Adunare(x, y);
		assertNotNull(rezultat.result);
		assertEquals(rezultat.result, " 1");
		nrTesteCuSucces++;
	}

	@Test
	public void testSub() {
		polin.clear();
		polin.tfPrimul.setText("7 0");
		polin.tfDoilea.setText("1 -1");
		String x = polin.tfPrimul.getText();
		String y = polin.tfDoilea.getText();
		Scadere rezultat = new Scadere(x, y);
		assertNotNull(rezultat.result);
		assertEquals(rezultat.result, " 6x+1");
		nrTesteCuSucces++;
	}

	@Test
	public void testSub2() {
		polin.clear();
		polin.tfPrimul.setText("1 1 1 1 1 1 1 1");
		polin.tfDoilea.setText("2 2 2 2 2 2 2 2");
		String x = polin.tfPrimul.getText();
		String y = polin.tfDoilea.getText();
		Scadere rezultat = new Scadere(x, y);
		assertNotNull(rezultat.result);
		assertEquals(rezultat.result, " -x^7-x^6-x^5-x^4-x^3-x^2-x-1");
		nrTesteCuSucces++;
	}

	@Test
	public void testDivide() {
		polin.clear();
		polin.tfPrimul.setText("7 0");
		polin.tfDoilea.setText("1 -1");
		String x = polin.tfPrimul.getText();
		String y = polin.tfDoilea.getText();
		Impartire rezultat = new Impartire(x, y);
		assertNotNull(rezultat.result);
		assertEquals(rezultat.result, " Catul este: 7.0 ; Restul este: 7.0");
		nrTesteCuSucces++;
	}

	@Test
	public void testDivide2() {
		polin.clear();
		polin.tfPrimul.setText("7 0");
		polin.tfDoilea.setText("0");
		String x = polin.tfPrimul.getText();
		String y = polin.tfDoilea.getText();
		Impartire rezultat = new Impartire(x, y);
		assertNotNull(rezultat.result);
		assertEquals(rezultat.result, "Nu se poate imparti la 0");
		nrTesteCuSucces++;
	}

	@Test
	public void tesDerivate() {
		polin.clear();
		polin.tfPrimul.setText("7 0");
		String x = polin.tfPrimul.getText();
		Derivare rezultat = new Derivare(x);
		assertNotNull(rezultat.result);
		assertEquals(rezultat.result, " 7");
		nrTesteCuSucces++;
	}

	@Test
	public void tesDerivate2() {
		polin.clear();
		polin.tfPrimul.setText("0");
		String x = polin.tfPrimul.getText();
		Derivare rezultat = new Derivare(x);
		assertNotNull(rezultat.result);
		assertEquals(rezultat.result, "0");
		nrTesteCuSucces++;
	}

	@Test
	public void tesIntegrate() {
		polin.clear();
		polin.tfPrimul.setText("7 0");
		String x = polin.tfPrimul.getText();
		Integrare rezultat = new Integrare(x);
		assertNotNull(rezultat.result);
		assertEquals(rezultat.result, " 3.5x^2+C");
		nrTesteCuSucces++;
	}

	@Test
	public void tesIntegrate2() {
		polin.clear();
		polin.tfPrimul.setText("7 ,, , ,    , 0  ,  ,,");
		String x = polin.tfPrimul.getText();
		Integrare rezultat = new Integrare(x);
		assertNotNull(rezultat.result);
		assertEquals(rezultat.result, " 3.5x^2+C");
		nrTesteCuSucces++;
	}

}
