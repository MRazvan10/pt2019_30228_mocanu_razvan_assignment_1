package Operatii;

public class Integrare {
	public int[] arr1 = new int[50];
	private int ok = 1;
	public String result = " ";
	private int size1 = 0;

	public Integrare(String polinom1) {
		for (int i = 0; i < polinom1.length(); i++) {
			if (!((polinom1.charAt(i) >= '0' && polinom1.charAt(i) <= '9') || polinom1.charAt(i) == ' '
					|| polinom1.charAt(i) == ',' || polinom1.charAt(i) == '-'))
				ok = 0;
		}
		if ((polinom1.charAt(0) < '0' || polinom1.charAt(0) > '9') && polinom1.charAt(0) != '-')
			ok = 0;
		if (ok == 1) {
			String[] monom1 = polinom1.split("[ ,]+");
			for (String monom : monom1) {
				arr1[size1] = Integer.parseInt(monom);
				size1++;
			}

			Integreaza(arr1);
		} else
			result = "Format invalid";

	}

	public void Integreaza(int[] s1) {
		float[] coef = new float[size1];
		int putere = size1;
		for (int i = 0; i < size1; i++) {
			coef[i] = (float) (s1[i] * 1.0 / (size1 - i));
		}
		for (int i = 0; i < size1 - 1; i++) {
			if (coef[i] != 0)
				result = result + coef[i] + "x^" + putere + "+";
			putere--;
		}
		if (coef[size1 - 1] != 0)
			result = result + coef[size1 - 1] + "x";
		if (result.equals(" "))
			result = "0";
		result = result.replaceAll("\\+-", "-");
		result = result.replaceAll("\\^1\\+", "+");
		result = result.replaceAll("\\^1\\-", "-");
		result = result.replaceAll("\\+1.0x", "+x");
		result = result.replaceAll("\\-1.0x", "-x");
		result = result.replaceAll("\\ 1.0x", " x");
		if (result.charAt(result.length() - 1) == '+')
			result = result.substring(0, result.length() - 1);
		result = result + "+C";
	}
}
